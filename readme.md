Language Switcher
=================

**Version 1.2.2** -- 2022-03-24

A simple Language Switcher component for Yii2 applications, designed for boostrap navbar use.
No widget, no ajax, no javascript.

Prerequisites
-------------

    yii2 >= 2.0.13
    php >= 5.6
    Bootstrap >= 3

Installation
------------

The preferred way to install this extension is through composer.

To install, either run

    composer require uhi67/yii2-languageswitcher "1.*" 

or add

    "uhi67/yii2-languageswitcher" : "^1.*"

or clone form github

    git clone https://github.com/uhi67/yii2-languageswitcher

Usage
-----

### Settings in config file (web.php)

Define selectable languages using canonical `ll-CC` notation. (However it works pretty well with language codes only)
An optional callback may be defined to perform on language selection, e.g. to save the language into the user record.  

    'components' => [
        ...
        'languageSwitcher' => [
            'class' => \uhi67\languageswitcher\LanguageSwitcher::class,
			'languages' => ['en-GB' => 'English', 'hu-HU' => 'Magyar'],
			'callback' => function($la) {
				if (!\Yii::$app->user->isGuest) {
					/** @var \app\models\AppUser $user */
					$user = \Yii::$app->user->identity;
					$user->lang = $la;
					$user->save();
				}
			}
		],
    ],
    'bootstrap' => [
		...
	    'languageSwitcher',
    ],
    
See all configuration options at class source. 

### Usage in the controller

Needed only if you are using uhi67\UhiMenu:

    $mainMenu = [
        ...
        Yii::$app->languageSwitcher->menuData($options),
    ];

Then, render menu as described in uhi67\UhiMenu
See options of menuData method at method source.

### Usage in views

    \uhi67\languageswitcher\LanguageSwitcher::register($this);

Then, only if you are using native yii2-bootstrap NavBar:

    <?php NavBar::begin(...); ?>
	<?= Nav::widget([
			'options' => ['class' => 'navbar-nav navbar-right'],

			'items' => [
			    ...
			    Yii::$app->languageSwitcher->navItems(),
			],
			'encodeLabels' => false,
		])
	?>
	<?php NavBar::end(); ?>

## Changes

### v1.2.2 -- 2022-03-24

- explicit bootstrap3 dependency removed
- flagname issue fixed

### v1.2.1

- options in `navItems()` method as well
- `flag` option is effective on submenu as well 
- `country` option added

### v1.2

- `flag` option is added

### v1.1

- `category` property is added 
- `options` parameter is added to `menuData()`
