<?php

namespace uhi67\languageswitcher;

use yii\web\AssetBundle;

/**
 * LanguageSwitcher asset bundle
 */
class LanguageSwitcherAsset extends AssetBundle {
	public $sourcePath = '@vendor/uhi67/yii2-languageswitcher/src/assets';
	public $css = [
        'flags/flags.min.css',
		'languageswitcher.css',
    ];
}
