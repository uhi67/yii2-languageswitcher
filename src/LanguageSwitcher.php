<?php
namespace uhi67\languageswitcher;

use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\AssetBundle;
use yii\web\View;

class LanguageSwitcher extends BaseObject {
	/** @var array $languages -- set of available languages e.g. ['hu'=>'Magyar', ...] */
	public $languages;
	/** @var callable $callback -- user callback on setting language function($la) */
	public $callback;
	/** @var string $category -- translation category for captions and titles */
	public $category = 'app';

	/**
	 * @var array countries
	 * Maps ISO 639-1 languages (without locale) to ISO 3166-1 country code
	 */
	public static $countries = [
		'en' => 'gb',
	];

	public function init() {
		// Language switch
		if($la = Yii::$app->request->get('la')) {
			Yii::$app->session->set('la', Yii::$app->language = $la);
			if(Yii::$app->controller) {
				if(is_callable($this->callback)) call_user_func($this->callback, $la);
				Yii::$app->response->redirect(Url::current(['la' => null]))->send();
			}
			return;
		}
		$la = Yii::$app->session->get('la');
		if($la) Yii::$app->language = $la;
	}

	/**
	 * Creates menu data for \uhi67\UhiMenu component
	 *
	 * Display options:
	 * - currentName (bool) -- display current language name in language menu item (default is true)
	 * - itemName (bool)  -- display language name in selectable items (default is true)
	 * - title (string) -- default is 'Select language' (translated to current language)
	 * - flag (bool) -- display flag (default is true)
	 * - country (bool) -- display country code with language code when no flag is displayed (default is false)
	 *
	 * @param array $options -- display options
	 * @return array
	 */
	public function menuData($options = ['flag'=>true]) {
		$request = Yii::$app->request;
		$languages = $this->languages;
		$currentName = ArrayHelper::getValue($options, 'currentName', true);
		$itemName = ArrayHelper::getValue($options, 'itemName', true);
		$showFlag = ArrayHelper::getValue($options, 'flag', true);
		$country = ArrayHelper::getValue($options, 'country', false);
		$lang = Yii::$app->language;
		/** @noinspection TranslationsCorrectnessInspection */
		$title = ArrayHelper::getValue($options, 'title', Yii::t($this->category, 'Select language'));
		$flagName = strtolower(substr($lang,3,2)); if($flagName=='') $flagName = strtolower(substr($lang,0,2));
		return [
			'enabled' => Yii::$app->request->isGet,
			'caption' => ($showFlag ? '<i class="'.'flag flag-'.$flagName.'"></i>':'') .
				($currentName ? ArrayHelper::getValue($languages, $lang, $lang) : '') .
				((!$currentName && !$showFlag) ? strtoupper($country ? $lang : substr($lang,0,2)) : '')
			,
			'title' => $title,
			'class' => 'language-switcher',
			'items' => array_map(function($la, $language) use($languages, $request, $itemName, $lang, $showFlag, $country) {
				$caption = $itemName ? $languages[$la] : '';
				$prefix = $showFlag ? '<i class="'.'flag flag-'.static::country($la).'"></i>' : strtoupper($country ? $la : substr($la, 0, 2));
				$dash = $caption && !$showFlag ? ' - ' : '';
				return [
					'enabled' => $la != $lang && $request->isGet,
					'caption' => $prefix.$dash.$caption,
					'title' => Yii::t('app', $language, [], $la),
					'action' => Url::current(['la'=>$la]),
				];
			},
				array_keys($this->languages),
				array_values($this->languages)
			),
		];
	}

	/**
	 * Returns an item array for Bootstrap NavBar
	 *
	 * Usage
	 *
	 * ```
	 *    <?= Nav::widget([
	 *    'options' => ['class' => 'navbar-nav navbar-right'],
	 *        'items' => [
	 *            ...
	 *            Yii::$app->languageSwitcher->navItems(),
	 *        ],
	 *        'encodeLabels' => false,
	 *    ]) ?>
	 * ```
	 *
	 * Display options:
	 * - currentName (bool) -- display current language name in language menu item (default is true)
	 * - itemName (bool)  -- display language name in selectable items (default is true)
	 * - title (string) -- default is 'Select language' (translated to current language)
	 * - flag (bool) -- display flag (default is true)
	 * - country (bool) -- display country code with language code when no flag is displayed (default is false)
	 *
	 * @param array $options -- display options
	 *
	 * @return array
	 */
	public function navItems($options = ['flag'=>true]) {
		$request = Yii::$app->request;
		$languages = $this->languages;
		$currentName = ArrayHelper::getValue($options, 'currentName', true);
		$itemName = ArrayHelper::getValue($options, 'itemName', true);
		$flag = ArrayHelper::getValue($options, 'flag', true);
		$country = ArrayHelper::getValue($options, 'country', false);

		$lang = Yii::$app->language;
		$countryCode = static::country($lang);
		$langName = ArrayHelper::getValue($languages, $lang, $lang);
		/** @noinspection TranslationsCorrectnessInspection */
		return [
			'label' => ($flag ? '<i class="'.'flag flag-'.$countryCode.'"></i>':'') .
				($currentName ? $langName : '') .
				((!$currentName && !$flag) ? strtoupper($country ? $lang : substr($lang,0,2)) : '')
			,
			'linkOptions' => ['title' => Yii::t($this->category, 'Select language')],
			'options'=>['class'=>"language-switcher"],
			'items' => array_map(function($la, $language) use($languages, $request, $itemName, $flag, $country) {
				$caption = $itemName ? $language : '';
				$prefix = $flag ? '<i class="'.'flag flag-'.static::country($la).'"></i>' : strtoupper($country ? $la : substr($la,0,2));
				$dash = $caption && !$flag ? ' - ' : '';
				return [
					'enabled' => $la != Yii::$app->language && $request->isGet,
					'label' => $prefix.$dash.$caption,
					'linkOptions' => [
						'title' => Yii::t('app', $language, [], $la),
						'class' => $la != Yii::$app->language && $request->isGet ? '' : 'disabled',
					],
					'url' => $la != Yii::$app->language && $request->isGet ? Url::current(['la'=>$la]) : '',
				];
			},
				array_keys($this->languages),
				array_values($this->languages)
			),
		];
	}

	/**
	 * @param View $view
	 * @return AssetBundle
	 * @throws
	 */
	public static function register($view) {
		return $view->registerAssetBundle(LanguageSwitcherAsset::class);
	}

	/**
	 * Determines ISO 3166-1 location code of an ISO/IEC 15897 locale or ISO 639-1 language code.
	 * If explicit mapping is not found, returns the alpha-2 language code.
	 * @param string $locale
	 * @return string -- the alpha-2 country code in lowercase
	 */
	public static function country($locale) {
		if(strlen($locale)==5) return strtolower(substr($locale,3));
		return ArrayHelper::getValue(static::$countries, $locale, $locale);
	}
}
